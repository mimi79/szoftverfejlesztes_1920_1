package com.example.learning_androidstudio

import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        //gombra kattintás után, a hello xy szöveg fog megjelenni a képernyőn(textViwe-ban).
        //xy: a felhasználó adja meg, az editText01 részben(plain text)
        //
        btn01.setOnClickListener {
            d("User", "push the button and the name appera in hello!")
            helloMsg.text="Hello ${editText01.text } !"
        }

        //hogyan hozzunk létre uj activity-t?
        //1: menjünk arra a mappára, ahol a MainActivity van és ott jobb egérgomb
        //2: new--ykotlin file/class
        //3: elnevezzük a cuccot és class kivélasztása
        //4: miután leokéztuk megnyilik az uj activity, átmegyünk rá

        //hogyan megyünk kövi activyre
        btn03.setOnClickListener{
            d("user_move_to_secondAct", "user went to the second activity")
            //this: mindig az amiről indulunk,
            // utána vesszővel elválasztva irjuk azt amire megyünkpl: SecondActivity::class.java (standerd)
            startActivity(Intent(this,SecondActivity::class.java))
        }

    }





}
