package com.example.learning_androidstudio

import android.os.Bundle
import android.util.Log.d
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_second.*

//hogyan hozzunk létre uj activity-t? folyt
//5: itt beirjuk a file neve után _> : AppCompatActivity()-->megmondom hogy uj felületről lesz majd szó
//6: belül alt+o-val megnyitünk egy kis menüt, majd az onCreate fgv-t meghivja elkezdjük
//7: csinálunk neki layout-ot: setContentView(R.layout.activity_second)
//8: hozzáadjuk a manifest állományhoz: sárga osztálynéven alt+enter és add manifest
class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        //minden esetben rögziteni kell a layouton a cuccokat, igy minden kpernyőn ugyan olyan helyen lesz
        //behuzunk egy buttont pl, és körülötte lévő kicsi köröket jobb-ball-fel-le irányba huzzunk rögzités céljából
        //ha egy A tárgyat vmi al akarjuk rögziteni, akkor annak a B dolognak az alsó pöttyéhez hozzunk az A tárgy felső pöttyét.
        btn02.setOnClickListener {
            d("user_helloMsg", "pushed btn02 on secind activity and hello msg appear.")
            helloMsg02.text="Hello ${editText02.text} on Second activity! "
        }
    }
}